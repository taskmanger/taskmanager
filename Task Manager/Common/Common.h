//
//  Common.h
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Common : UIViewController

+(void)addBackButtonOnNavigationController:(UINavigationController *)navController withTitle:(NSString *)title;
+(NSString *)getFormattedDate:(NSString *)strDate withFormat:(NSString *)strFormat;
+(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message andController:(UIViewController *)controller;
@end
