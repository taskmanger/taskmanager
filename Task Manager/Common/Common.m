//
//  Common.m
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import "Common.h"

@implementation Common

+(void)addBackButtonOnNavigationController:(UINavigationController *)navController withTitle:(NSString *)title
{
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    [barButton setTitle:@""];
    //[barButton setTintColor:[UIColor greenColor]];
    [navController.navigationBar.topItem setBackBarButtonItem:barButton];
    [navController setNavigationBarHidden:NO animated:NO];
}

+(NSString *)getFormattedDate:(NSString *)strDate withFormat:(NSString *)strFormat
{
    NSRange range = [strDate rangeOfString:@" "];
    strDate = [strDate substringToIndex:range.location]; /// here this is the date with format yyyy-MM-dd
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
    NSDate *date = [dateFormatter dateFromString: strDate]; // here you can fetch date from string with define format
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:strFormat]; //@"MMM dd, yyyy" // here set format which you want...
    NSString *convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
    return convertedString;
}

+(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message andController:(UIViewController *)controller
{
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[alert show];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [controller presentViewController:alert animated:YES completion:nil];
}
@end
