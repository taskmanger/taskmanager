//
//  User.m
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import "User.h"

@implementation User{
    // Private instance variables
    //
}

- (id)init {
    self = [super init];
    if (self) {
        // Any custom setup work goes here
        _name = nil;
    }
    return self;
}

- (id)initWithUser:(User *)aUser {
    self = [super init];
    if (self) {
        // Any custom setup work goes here
        _name = [aUser.name copy];
    }
    return self;
}
@end
