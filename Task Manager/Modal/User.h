//
//  User.h
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject{
    // Protected instance variables (not recommended)
}

@property (copy) NSString *name;
@end
