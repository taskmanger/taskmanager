//
//  WaterVC.h
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaterVC : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, UICollectionViewDataSource, UICollectionViewDelegate>{
    IBOutlet UITextField *txtNoOfCupsPerDay;
    IBOutlet UICollectionView *collectionViewCups;
    
    NSArray *dataArray;
    NSMutableArray *arrCupsInDay;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

-(IBAction)selectTotalCupsInDay:(id)sender;
-(IBAction)save:(id)sender;
@end
