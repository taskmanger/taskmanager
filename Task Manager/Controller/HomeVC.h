//
//  HomeVC.h
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@end
