//
//  SleepVC.h
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SleepVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate>{
    IBOutlet UITextField *txtSleepDate, *txtSleepTime;
    IBOutlet UITableView *tblSleepTimes;
    
    NSArray *arrWeekDays;
    NSMutableArray *arrSleepTimes;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

-(IBAction)selectDate:(id)sender;
-(IBAction)selectTime:(id)sender;
@end
