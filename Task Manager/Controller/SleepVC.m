//
//  SleepVC.m
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import "SleepVC.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "Common.h"

@implementation SleepVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    arrWeekDays = @[@"Sunday", @"Monday", @"Tuesday", @"Wedness", @"Thursday", @"Friday", @"Saturday"];
    arrSleepTimes = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self getSleepTimes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getSleepTimes{
    AppDelegate* sharedDelegate = [AppDelegate appDelegate];
    
    NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Sleep" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // Handle the error.
        NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
    }
    else{
        NSLog(@"%@", fetchedObjects);
        arrSleepTimes = [fetchedObjects mutableCopy];
        [tblSleepTimes reloadData];
    }
}

#pragma mark - UITableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrSleepTimes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell.imageView setImage:[UIImage imageNamed:@"alarm"]];
    [cell.textLabel setText:[[arrSleepTimes objectAtIndex:indexPath.row] valueForKey:@"sleepDay"]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.detailTextLabel setText:[[arrSleepTimes objectAtIndex:indexPath.row] valueForKey:@"sleepTime"]];
    [cell.detailTextLabel setTextColor:[UIColor whiteColor]];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        [arrSleepTimes removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
    }
}

#pragma mark - UIActions

-(IBAction)selectDate:(id)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SELECT A DAY\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(-8, 44, 320, 200)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    //[picker selectRow:0 inComponent:0 animated:YES];
    
    [alertController.view addSubview:picker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSLog(@"OK");
        }];
        action;
    })];
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
}

-(IBAction)selectTime:(id)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SELECT A TIME\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(-8, 44, 320, 200)];
    [picker setDatePickerMode:UIDatePickerModeTime];
    //NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    //[picker setLocale:locale];
    
    [alertController.view addSubview:picker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //NSLog(@"OK");
            NSLog(@"Sleep Time: %@",picker.date);
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"hh:mm a"];
            NSString *nsstr = [format stringFromDate:picker.date];
            
            [txtSleepTime setText:nsstr];
        }];
        action;
    })];
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
}


-(IBAction)save:(id)sender{
    NSString *message = @"";
    if (txtSleepDate.text.length <= 0){
        message = [message stringByAppendingString:@"\nSelect sleep day."];
    }
    if (txtSleepTime.text.length <= 0) {
        message = [message stringByAppendingString:@"\nSelect sleep time."];
    }
    
    if ([message isEqualToString:@""]){
        AppDelegate* sharedDelegate = [AppDelegate appDelegate];
        
        NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Sleep" inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        [fetchRequest setResultType:NSDictionaryResultType];
        
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if (fetchedObjects == nil) {
            // Handle the error.
            NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
        }
        else{
            NSManagedObject *setup = [NSEntityDescription insertNewObjectForEntityForName:@"Sleep" inManagedObjectContext:context];
            [setup setValue:txtSleepDate.text forKey:@"sleepDay"];
            [setup setValue:txtSleepTime.text forKey:@"sleepTime"];
            
            // Save the context
            NSError *error = nil;
            if (![context save:&error]) {
                NSLog(@"Saving Failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
            }
            else{
                // schedule notification for sleep time every day
                // get hour, minues, and weekday and create date
                NSArray *arr = [[NSArray alloc] init];
                arr = [txtSleepTime.text componentsSeparatedByString:@":"];
                NSString *hour = [arr objectAtIndex:0];
                NSString *minute = [[[arr objectAtIndex:1] componentsSeparatedByString:@" "] objectAtIndex:0];
                NSInteger weekday = 2; // Monday by default
                for (int i=0; i<arrWeekDays.count; i++) {
                    if ([txtSleepDate.text isEqualToString:[arrWeekDays objectAtIndex:i]]) {
                        weekday = i+1;
                        break;
                    }
                }
                
                unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitWeekOfYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;
                
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateComponents *dateComponets = [calendar components: unitFlags fromDate:[NSDate date]];
                
                dateComponets.weekday = weekday;
                dateComponets.hour = hour.intValue;
                dateComponets.minute = minute.intValue;
                dateComponets.second = 00;
                dateComponets.timeZone = [NSTimeZone defaultTimeZone];
                
                NSDate *fireDate = [calendar dateFromComponents:dateComponets];
                
                // create alert message
                NSString *alertMessage = [NSString stringWithFormat:@"It's your sleep time!!"];
                
                // Schedule the notification
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = fireDate;
                localNotification.alertBody = alertMessage;
                localNotification.alertAction = @"OK";
                localNotification.repeatInterval = NSCalendarUnitWeekOfYear;
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
                [txtSleepDate setText:@""];
                [txtSleepTime setText:@""];
                [self getSleepTimes];
                
                NSLog(@"Setup data saved!!");
                NSLog(@"Sleep: %@", fetchedObjects);
                //[self.navigationController popViewControllerAnimated:YES];
                [Common showAlertWithTitle:@"Alert" andMessage:@"Your sleep time saved now." andController:self];
            }
        }
    }
    else{
        [Common showAlertWithTitle:@"Alert" andMessage:message andController:self];
    }
}

#pragma mark - UIPickerViewDelegates

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrWeekDays.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [txtSleepDate setText:[arrWeekDays objectAtIndex:row]];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 100)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [arrWeekDays objectAtIndex:row];

    return label;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 200;
}

@end
