//
//  WorkVC.h
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate>{
    IBOutlet UITextField *txtWorkDay, *txtWorkTime;
    IBOutlet UITableView *tblWorkTimes;
    
    NSArray *arrWeekDays;
    NSMutableArray *arrWorkTimes;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

-(IBAction)selectDate:(id)sender;
-(IBAction)selectTime:(id)sender;
@end
