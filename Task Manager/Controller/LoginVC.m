//
//  LoginVC.m
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import "LoginVC.h"
#import "AppDelegate.h"
#import "Common.h"
#import "SWRevealViewController.h"

@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Do any additional setup after loading the view, typically from a nib.
    
    AppDelegate* sharedDelegate = [AppDelegate appDelegate];
    
    NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    //[fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setPropertiesToFetch:@[@"name"]];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // Handle the error.
        NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
    }
    else{
        //NSLog(@"%@", fetchedObjects);
        if (fetchedObjects.count>0) {
            [txtUserName setText:[[fetchedObjects objectAtIndex:0] valueForKey:@"name"]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)saveName:(id)sender{
    if(![txtUserName.text isEqualToString:@""]){
        AppDelegate* sharedDelegate = [AppDelegate appDelegate];
        
        NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        [fetchRequest setResultType:NSDictionaryResultType];
        //[fetchRequest setReturnsDistinctResults:YES];
        [fetchRequest setPropertiesToFetch:@[@"name"]];
        
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if (fetchedObjects == nil) {
            // Handle the error.
            NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
        }
        else{
            //NSLog(@"%@", fetchedObjects);
            // if user name not saved then save else goto home screen
            if (fetchedObjects.count <= 0) {
                NSManagedObject *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
                [user setValue:txtUserName.text forKey:@"name"];
                
                // Save the context
                NSError *error = nil;
                if (![context save:&error]) {
                    NSLog(@"Saving Failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
                }
                else{
                    NSLog(@"User name saved!!");
                    //goto home screen
                    [self performSegueWithIdentifier:@"revealViewController" sender:self];
                }
            }
            else{
                // goto home screen
                [self performSegueWithIdentifier:@"revealViewController" sender:self];
            }
        }
    }
    else{
        [Common showAlertWithTitle:@"Alert" andMessage:@"Please enter username" andController:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"home"])
    {
        //HomeVC *home = (HomeVC*)[segue destinationViewController];
    }
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == txtUserName){
        [txtUserName resignFirstResponder];
    }
    return YES;
}
@end
