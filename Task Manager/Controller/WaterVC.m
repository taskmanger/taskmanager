//
//  WaterVC.m
//  Task Manager
//
//  Created by Task Manager on 23/04/16.
//  Copyright © 2016 TaskManagerTeam. All rights reserved.
//

#import "WaterVC.h"
#import "SWRevealViewController.h"
#import "Common.h"

@interface WaterVC ()

@end

@implementation WaterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    // Create array to select no of cups to drink in a day
    arrCupsInDay = [[NSMutableArray alloc] init];
    for (int i=2; i<=24; i++) {
        //int j = i+2;
        [arrCupsInDay addObject:[NSString stringWithFormat:@"%d", i++]];
    }
    
    // Create array of records to show in collection view in multiple sections
    NSMutableArray *firstSection = [[NSMutableArray alloc] init];
    for (int i=0; i<[txtNoOfCupsPerDay.text integerValue]; i++) {
        [firstSection addObject:[NSString stringWithFormat:@"Cell %d", i]];
    }
    
    dataArray = [[NSArray alloc] initWithObjects:firstSection, nil];
    [collectionViewCups reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Get value from local storage
    NSLog(@"noOfCupsPerDay: %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"noOfCupsPerDay"]);
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"noOfCupsPerDay"]) {
        [txtNoOfCupsPerDay setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"noOfCupsPerDay"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //    if ([[segue identifier] isEqualToString:@"home"])
    //    {
    //        //HomeVC *home = (HomeVC*)[segue destinationViewController];
    //    }
}

#pragma mark - IBActions

- (IBAction)selectTotalCupsInDay:(id)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SELECT NO. OF CUPS\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(-8, 44, 320, 200)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    //[picker selectRow:0 inComponent:0 animated:YES];
    
    [alertController.view addSubview:picker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSLog(@"OK");
        }];
        action;
    })];
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
}

- (IBAction)save:(id)sender{
    int noOfCupsPerDay = [txtNoOfCupsPerDay.text integerValue];
    if (noOfCupsPerDay > 0) {
        // Store value in local storage
        [[NSUserDefaults standardUserDefaults] setObject:txtNoOfCupsPerDay.text forKey:@"noOfCupsPerDay"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Save number of cups to drink daily in local storage
        int noOfCups = [txtNoOfCupsPerDay.text integerValue];
        int noOfHours = 24/noOfCups; // notification after every x hours to drink water
        
        // Schedule notification for every x hours
        for (int i=0; i<noOfCups; i++) {
            NSDate *fireDate = [NSDate dateWithTimeIntervalSinceNow:noOfHours*60*60];
            
            // Create alert message
            NSString *alertMessage = [NSString stringWithFormat:@"It's time to drink water!!"];
            
            // Schedule the notification
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = fireDate;
            localNotification.alertBody = alertMessage;
            localNotification.alertAction = @"OK";
            localNotification.repeatInterval = NSCalendarUnitWeekOfYear;
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
    }
    else{
        [Common showAlertWithTitle:@"Alert" andMessage:@"Please enter number of cups to drink daily." andController:self];
    }
}

#pragma mark - UIPickerViewDelegates

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrCupsInDay.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [txtNoOfCupsPerDay setText:[arrCupsInDay objectAtIndex:row]];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 100)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [arrCupsInDay objectAtIndex:row];
    
    return label;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 200;
}

#pragma mark - UICollectionViewDelegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [dataArray count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSMutableArray *sectionArray = [dataArray objectAtIndex:section];
    return [sectionArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //NSMutableArray *data = [dataArray objectAtIndex:indexPath.section];
    //NSString *cellData = [data objectAtIndex:indexPath.row];
    
    static NSString *cellIdentifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
    [imageView setImage:[UIImage imageNamed:@"glass"]];
    return cell;
}

@end